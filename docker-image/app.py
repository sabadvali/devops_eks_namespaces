from flask import Flask, jsonify
import psutil
import datetime

app = Flask(__name__)

@app.route('/')
def get_system_info():
    try:
        current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        cpu_usage_percent = psutil.cpu_percent(interval=1)
        memory_usage = psutil.virtual_memory()
        total_memory = memory_usage.total
        available_memory = memory_usage.available
        used_memory = memory_usage.used
        memory_usage_percent = memory_usage.percent
    except:
        return "Error while getting system info"
    
    return jsonify({
        'current_time': current_time,
        'cpu_usage_percent': cpu_usage_percent,
        'total_memory': total_memory,
        'available_memory': available_memory,
        'used_memory': used_memory,
        'memory_usage_percent': memory_usage_percent
    })


if __name__ == '__main__':
    app.run(host="0.0.0.0",port=8888)



